# Makefile for C application
# Copyright � 2010, RISC OS Open
# All rights reserved.

COMPONENT ?= rel2html

include Makefiles:StdTools
include Makefiles:StdRules
include Makefiles:AppLibs
include Makefiles:CApp

# Dynamic dependencies:
